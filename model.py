import numpy as np


class Net:
    def __init__(self, weights, biases):
        """
        weights list of weights
        input: 46
        output: 22
        """
        self.weights = weights
        self.biases = biases

    def __call__(self, x):
        for weight, bias in zip(self.weights, self.biases):
            x = np.tanh(np.dot(x, weight) + bias)

        return x
