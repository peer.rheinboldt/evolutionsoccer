import numpy as np

from env import SoccerGame
from model import Net

SG = SoccerGame()


def compete(team1, team2, show=False):
    SG.reset()
    while SG.winner == None and SG.step < 180:
        team1_decisions = team1([np.concatenate((SG.team1.reshape(-1), SG.team2.reshape(-1), SG.ball_pos))])
        team2_decisions = team2([np.concatenate((SG.team2.reshape(-1), SG.team1.reshape(-1), SG.ball_pos))])
        SG.next_step(team1_decisions, team2_decisions)
        if show:
            SG.show()
    rewards = [SG.team1_contacts * 0.01, SG.team2_contacts * 0.01]
    if SG.winner != None:
        rewards[SG.winner] += 100
    return rewards


def next_generation(population, n_surviving):
    population_winners = np.zeros(len(population))
    for i in range(len(population)):
        if i == len(population) - 1:
            rewards = compete(population[i], population[0])
            population_winners[i] += rewards[0]
            population_winners[0] += rewards[1]
        else:
            rewards = compete(population[i], population[i + 1])
            population_winners[i] += rewards[0]
            population_winners[i + 1] += rewards[1]

    surviving_indexes = np.argsort(population_winners)[-n_surviving:]
    surviving_teams = population[surviving_indexes]
    new_population = []
    for _ in range(len(population)):
        parents = surviving_teams[np.random.randint(0, n_surviving, 2)]
        weights = np.asarray(
            [
                [
                    [
                        parents[np.random.randint(0, 2)].weights[i][j][k] + np.random.randn()
                        for k in range(len(parents[0].weights[i][j]))
                    ]
                    for j in range(len(parents[0].weights[i]))
                ]
                for i in range(len(parents[0].weights))
            ]
        )
        biases = np.asarray(
            [
                [
                    parents[np.random.randint(0, 2)].biases[i][j] + np.random.randn()
                    for j in range(len(parents[0].biases[i]))
                ]
                for i in range(len(parents[0].biases))
            ]
        )
        new_population.append(Net(weights, biases))
    print(population_winners.max(), population_winners.mean(), population_winners.min())  # rename to rewards
    return np.asarray(new_population)


def main(n_population, n_surviving):
    population = np.asarray(
        [
            Net(
                [np.random.randn(46, 32) * 0.05, np.random.randn(32, 22) * 0.05],
                [np.random.randn(32) * 0.05, np.random.randn(22) * 0.05],
            )
            for _ in range(n_population)
        ]
    )
    for i in range(1000):
        if i % 5 == 0:
            compete(population[0], population[1], True)
        population = next_generation(population, n_surviving)


if __name__ == "__main__":
    main(200, 10)

