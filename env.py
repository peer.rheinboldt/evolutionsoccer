# The Environment in which the AI plays

import numpy as np
import matplotlib.pyplot as plt

plt.ion()


class SoccerGame:
    def __init__(self):
        self.team1 = np.array([[1, i * 10 / 11] for i in range(1, 12)])
        self.team2 = np.array([[19, i * 10 / 11] for i in range(1, 12)])
        self.team1_contacts = 0
        self.team2_contacts = 0
        self.ball_pos = np.array([10.0, 5.0]).astype("float64")
        self.ball_acceleration = np.array([0.0, 0.0])
        self.winner = None
        self._start_plot()
        self.step = 0

    def reset(self):
        self.team1 = np.array([[1, i * 10 / 11] for i in range(1, 12)])
        self.team2 = np.array([[19, i * 10 / 11] for i in range(1, 12)])
        self.team1_contacts = 0
        self.team2_contacts = 0
        self.ball_pos = np.array([10.0, 5.0]).astype("float64")
        self.ball_acceleration = np.array([0.0, 0.0])
        self.winner = None
        self.step = 0

        self.team1_plot_pos = [[], []]
        self.team2_plot_pos = [[], []]
        self.ball_plot_pos = [[], []]

    def next_step(self, t1_decisions, t2_decisions):
        self.step += 1

        team1 = self.team1 + t1_decisions.reshape(11, 2)
        team1[:, 0] = np.maximum(0, np.minimum(20, team1[:, 0]))
        team1[:, 1] = np.maximum(0, np.minimum(10, team1[:, 1]))

        team2 = self.team2 + t2_decisions.reshape(11, 2)
        team2[:, 0] = np.maximum(0, np.minimum(20, team2[:, 0]))
        team2[:, 1] = np.maximum(0, np.minimum(10, team2[:, 1]))

        self._change_ball_acceleration(team1, team2)
        self.ball_pos += self.ball_acceleration

        if 0.5 > self.ball_pos[0] and 3 < self.ball_pos[1] < 7:
            print("GOAL!!!")
            self.winner = 1
        elif 19.5 < self.ball_pos[0] and 3 < self.ball_pos[1] < 7:
            self.winner = 0
            print("GOAL!!!")

        self.team1 = team1
        self.team2 = team2

    def show(self):
        self.team1_plot_pos = [self.team1[:, 0], self.team1[:, 1]]
        self.team1_plot.set_offsets(np.c_[self.team1_plot_pos[0], self.team1_plot_pos[1]])

        self.team2_plot_pos = [self.team2[:, 0], self.team2[:, 1]]
        self.team2_plot.set_offsets(np.c_[self.team2_plot_pos[0], self.team2_plot_pos[1]])

        self.ball_plot_pos = [self.ball_pos[0], self.ball_pos[1]]
        self.ball_plot.set_offsets(np.c_[self.ball_plot_pos[0], self.ball_plot_pos[1]])

        self.fig.canvas.draw_idle()
        plt.pause(0.01)

    def _change_ball_acceleration(self, team1, team2):
        player_pos = np.concatenate((team1, team2))
        old_player_pos = np.concatenate((self.team1, self.team2))
        player_collision_with_ball = np.where(
            ([player_pos[:, 0]] > (self.ball_pos[0] - 0.4))
            & ([player_pos[:, 0]] < (self.ball_pos[0] + 0.4))
            & ([player_pos[:, 1]] > (self.ball_pos[1] - 0.4))
            & ([player_pos[:, 1]] < (self.ball_pos[1] + 0.4))
        )
        if len(player_collision_with_ball[0]) > 0:
            self.team1_contacts += len(np.where(player_collision_with_ball[0] < 11)[0])
            self.team2_contacts += len(np.where(player_collision_with_ball[0] > 10)[0])
            directions = np.array(
                [
                    [player_pos[i, 0] - old_player_pos[i, 0], player_pos[i, 1] - old_player_pos[i, 1]]
                    for i in player_collision_with_ball
                ]
            )
            self.ball_acceleration = np.array([np.mean(directions[:, 0]), np.mean(directions[:, 1])])
        else:
            self.ball_acceleration = self.ball_acceleration * 9 / 10

    def _start_plot(self):
        self.fig, self.ax = plt.subplots()

        self.team1_plot_pos = [[], []]
        self.team1_plot = self.ax.scatter(self.team1_plot_pos[0], self.team1_plot_pos[1], c=[[1, 0, 0]], s=70)

        self.team2_plot_pos = [[], []]
        self.team2_plot = self.ax.scatter(self.team2_plot_pos[0], self.team2_plot_pos[1], c=[[0, 0, 1]], s=70)

        self.ball_plot_pos = [[], []]
        self.ball_plot = self.ax.scatter(self.ball_plot_pos[0], self.ball_plot_pos[1], c=[[0, 0, 0]], s=100)
        plt.xlim(0, 20)
        plt.ylim(0, 10)

        plt.draw()


if __name__ == "__main__":
    sg = SoccerGame()
    sg.show()
